﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class Player_Control : MonoBehaviour {

    NavMeshAgent agent;
	public Image[] Elementos_img = new Image[4];
	public Image[] ElementosLast_img = new Image [4];
	public ParticleSystem[] Elementos_PS = new ParticleSystem[3];
	public GameObject[] Elementos_PS2 = new GameObject[2];
    public float[] CD_Habilidades = new float[4];
	public string PulsaLast;
	public bool PulsaQ = false;
	public bool PulsaW = false;
	public bool PulsaE = false;
	public bool PulsaR = false;
	
	public bool Pulsado = false;
	public int Lanzar_rocas_potencia;
	public float Elemento_roca_CDfloat;
	public bool Elemento_roca_CDbool;


	void Start () {
		agent = GetComponent<NavMeshAgent>();
	}
	
	void Update () {
		//Digo los colores alpha de 0(invisible) y 1(visible)
		var alpahcolor0 = Elementos_img[0].color;
		var alphacolor1 = Elementos_img[0].color;
		alpahcolor0.a = 0.5f;
		alphacolor1.a = 1f;


		//Movimiento con clicks
		if(Input.GetMouseButton(0)){
			RaycastHit hit;

			if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 100)) {
				agent.destination = hit.point; 

			}
		}
		//Calcula el CD de lanzar rocas
		if(Elemento_roca_CDbool){
			Elemento_roca_CDfloat-=Time.deltaTime;
		}
		if(Elemento_roca_CDfloat<=0){
			Elemento_roca_CDbool=false;
			Elemento_roca_CDfloat=1f;
		}
		//Recibir los botones QWER
		if(Input.GetKey(KeyCode.Q)){
			PulsaQ = true;
			Pulsado = true;
			PulsaLast="Q";
			ElementosLast_img[0].enabled = true;
			ElementosLast_img[1].enabled = false;
			ElementosLast_img[2].enabled = false;
			ElementosLast_img[3].enabled = false;

		} else{
			PulsaQ = false;	
		}
		if(Input.GetKey(KeyCode.W)){
			PulsaW = true;
			Pulsado = true;
			PulsaLast="W";
			ElementosLast_img[0].enabled = false;
			ElementosLast_img[1].enabled = true;
			ElementosLast_img[2].enabled = false;
			ElementosLast_img[3].enabled = false;
			
		} else{
			PulsaW = false;
		}
		if(Input.GetKey(KeyCode.E)){
			PulsaE = true;
			Pulsado = true;
			PulsaLast="E";
			ElementosLast_img[0].enabled = false;
			ElementosLast_img[1].enabled = false;
			ElementosLast_img[2].enabled = true;
			ElementosLast_img[3].enabled = false;
		} else{
			PulsaE = false;
		}
		if(Input.GetKey(KeyCode.R)){
			PulsaR = true;
			Pulsado = true;
			PulsaLast="R";
			ElementosLast_img[0].enabled = false;
			ElementosLast_img[1].enabled = false;
			ElementosLast_img[2].enabled = false;
			ElementosLast_img[3].enabled = true;
		} else{
			PulsaR = false;
		}

		//Cambiar el alpha de QWER 
		if(PulsaQ){
			Elementos_img[0].color = alpahcolor0;
		} else{
			Elementos_img[0].color = alphacolor1;
		} 

		if(PulsaW){
			Elementos_img[1].color = alpahcolor0;
		} else{
			Elementos_img[1].color = alphacolor1;
		}

		if(PulsaE){
			Elementos_img[2].color = alpahcolor0;
		} else{
			Elementos_img[2].color = alphacolor1;
		} 
		
		if(PulsaR){
			Elementos_img[3].color = alpahcolor0;
		} else{
			Elementos_img[3].color = alphacolor1;
		}

		//habilidades QWER
		if((Pulsado)&&(Input.GetMouseButtonDown(1))){
			if(PulsaLast=="Q"){
				if(!Elementos_PS[0].isPlaying){
					Elementos_PS[0].Play();
				}
			}
			if(PulsaLast=="W"){
				if(!Elementos_PS[1].isPlaying){
					Elementos_PS[1].Play();
				}
			}
			if((PulsaLast=="E")&&(!Elemento_roca_CDbool)){
				GameObject bullet = Instantiate(Elementos_PS2[0], Elementos_PS2[1].transform.position,Elementos_PS2[1].transform.rotation) as  GameObject;
				bullet.GetComponent<Rigidbody>().AddForce(transform.forward * Lanzar_rocas_potencia);
				Elemento_roca_CDbool=true;
			}
			if(PulsaLast=="R"){
				if(!Elementos_PS[2].isPlaying){
					Elementos_PS[2].Play();
				}
			}
			
			Pulsado=false;
			PulsaLast="";
			ElementosLast_img[0].enabled = false;
			ElementosLast_img[1].enabled = false;
			ElementosLast_img[2].enabled = false;
			ElementosLast_img[3].enabled = false;
		}
		if(Input.GetKeyDown(KeyCode.C)){
			Pulsado=false;
			PulsaLast="";
			ElementosLast_img[0].enabled = false;
			ElementosLast_img[1].enabled = false;
			ElementosLast_img[2].enabled = false;
			ElementosLast_img[3].enabled = false;
		}
	}
}
