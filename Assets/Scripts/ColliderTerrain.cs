﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderTerrain : MonoBehaviour {

	public Rigidbody RB;
	public float Tiempo_gravedad;
	public float Tiempo_rebote;
	public bool choque;

	// Use this for initialization
	void Start () {
		RB = this.gameObject.GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
		//calcula cuando debe activarsse la gravedad
		Tiempo_gravedad-=Time.deltaTime;
		if(Tiempo_gravedad<=0){
			RB.useGravity=true;
		}
		//destruye cuando se acaba el tiempo de vida
		if(Tiempo_rebote<=0){
			Destroy(this.gameObject);

		}
		//empieza a contar desde que cae al suelo
		if(choque){
			Tiempo_rebote-=Time.deltaTime;
			RB.velocity=new Vector3(RB.velocity.x/1.03f,RB.velocity.y/1.03f,RB.velocity.z/1.03f);

		}

		//friccion mientras no hay gravedad
		if(!RB.useGravity){
			RB.velocity=new Vector3(RB.velocity.x/1.05f,RB.velocity.y/1.05f,RB.velocity.z/1.05f);
			this.gameObject.transform.localScale = new Vector3(this.gameObject.transform.localScale.x*1.01f,this.gameObject.transform.localScale.y*1.01f,this.gameObject.transform.localScale.z*1.01f);
		}
	}

	void OnCollisionEnter(Collision other){
		if(other.gameObject.tag == "Terrain"){
			choque=true;
		}
		
	}
}
